package io.farel.core;

import lombok.Getter;
import lombok.SneakyThrows;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

@Getter
public class ApplicationProperties {

    private static ApplicationProperties applicationProperties;

    /**
     * URL UI тестируемой системы
     */
    private String uiUrl;

    /**
     * URL API тестируемой системы
     */
    private String apiUrl;

    /**
     * URL Selenoid
     */
    private String remoteUrl;

    /**
     * Таймаут ожидания элемента
     */
    private Integer timeout;

    /**
     * Запуск в режиме headless
     */
    private Boolean isHeadless;


    /**
     * Запуск в режиме headless
     */
    private Boolean isRemote;

    /**
     * Размер окна браузера
     */
    private String browserSize;

    /**
     * Тип браузера (Chrome, Firefox и тд)
     */
    private String browser;

    /**
     * Логин пользователя
     */
    private String userLogin;

    /**
     * Пароль пользователя
     */
    private String userPassword;

    public ApplicationProperties() {
        loadData();
    }

    public static ApplicationProperties getInstance() {
        return ApplicationProperties.applicationProperties == null
                ? ApplicationProperties.applicationProperties = new ApplicationProperties()
                : ApplicationProperties.applicationProperties;
    }

    @SneakyThrows
    private void loadData() {
        InputStream fileInputStream = new FileInputStream("src/main/resources/application.properties");
        Properties properties = new Properties();
        properties.load(fileInputStream);
        uiUrl = String.valueOf(properties.getProperty("url.ui"));
        apiUrl = String.valueOf(properties.getProperty("url.api"));
        remoteUrl = String.valueOf(properties.getProperty("url.remote"));
        timeout = Integer.valueOf(properties.getProperty("ui.timeout"));
        isHeadless = Boolean.valueOf(properties.getProperty("ui.is.headless"));
        isRemote = Boolean.valueOf(properties.getProperty("url.is.remote"));
        browserSize = String.valueOf(properties.getProperty("ui.browser.size"));
        browser = String.valueOf(properties.getProperty("ui.browser"));
        userLogin = String.valueOf(properties.getProperty("user.login"));
        userPassword = String.valueOf(properties.getProperty("user.password"));

    }

}

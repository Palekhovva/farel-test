package io.farel.function.factorial.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum RequestParamEnum {

    NUMBER("number");

    private String value;
}

package io.farel.ui.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class FactorialTestDataDto {

    /**
     * Данные для расчета
     */
    private String dataForCalculate;

    /**
     * Ожидаемое сообщение об результатах расчета
     */
    private String expectedResultMessage;

}

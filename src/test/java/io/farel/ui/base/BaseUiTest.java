package io.farel.ui.base;

import com.codeborne.selenide.Configuration;
import io.farel.core.ApplicationProperties;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;

import static com.codeborne.selenide.Selenide.open;

public class BaseUiTest {
    private static final ApplicationProperties applicationProperties = ApplicationProperties.getInstance();

    @BeforeAll
    public static void beforeAll() {
        Configuration.browser = applicationProperties.getBrowser();
        Configuration.timeout = applicationProperties.getTimeout();
        Configuration.browserSize = applicationProperties.getBrowserSize();
        Configuration.headless = applicationProperties.getIsHeadless();
        remoteRun();
        Configuration.baseUrl = applicationProperties.getUiUrl();
    }

    @BeforeEach
    public void beforeEach() {
        open("");
    }

    private static void remoteRun() {
        if (applicationProperties.getIsRemote()) {
            Configuration.remote = applicationProperties.getRemoteUrl();
        }
    }

}

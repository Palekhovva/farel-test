package io.farel.ui.steps;

import com.codeborne.selenide.Condition;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Selenide.$;

public class FactorialUiSteps {

    @Step("Добавление данных в поле ввода числа для расчета факториала. Данные: {0}")
    public void addDataToFactorialInput(String text) {
        $("#number").shouldBe(Condition.visible).val(text);
    }

    @Step("Нажатие на кнопку расчета факториала")
    public void clickToCalculateButton() {
        $("#getFactorial").shouldBe(Condition.visible).click();
    }

    @Step("Проверка сообщения об расчете")
    public void checkResultMessage(String message) {
        $("#resultDiv").shouldBe(Condition.visible).shouldHave(Condition.text(message));
    }
}

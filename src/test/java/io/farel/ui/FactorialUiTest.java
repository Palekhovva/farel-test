package io.farel.ui;

import io.farel.ui.base.BaseUiTest;
import io.farel.ui.dto.FactorialTestDataDto;
import io.farel.ui.steps.FactorialUiSteps;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;


public class FactorialUiTest extends BaseUiTest {
    private final FactorialUiSteps factorialSteps = new FactorialUiSteps();

    @ParameterizedTest
    @MethodSource("getTestData")
    public void factorialUiTest(FactorialTestDataDto factorialTestDataDto) {
        factorialSteps.addDataToFactorialInput(factorialTestDataDto.getDataForCalculate());
        factorialSteps.clickToCalculateButton();
        factorialSteps.checkResultMessage(factorialTestDataDto.getExpectedResultMessage());
    }


    private static Stream<FactorialTestDataDto> getTestData() {
        return Stream.of(
                FactorialTestDataDto.builder()
                        .dataForCalculate("-1")
                        .expectedResultMessage("The factorial of 0 is: 1")
                        .build(),
                FactorialTestDataDto.builder()
                        .dataForCalculate("0")
                        .expectedResultMessage("The factorial of 0 is: 1")
                        .build(),
                FactorialTestDataDto.builder()
                        .dataForCalculate("170")
                        .expectedResultMessage("The factorial of 170 is: 7.257415615307999e+306")
                        .build(),
                FactorialTestDataDto.builder()
                        .dataForCalculate("171")
                        .expectedResultMessage("The factorial of 171 is: Infinity")
                        .build(),
                FactorialTestDataDto.builder()
                        .dataForCalculate("null")
                        .expectedResultMessage("Please enter an integer")
                        .build(),
                FactorialTestDataDto.builder()
                        .dataForCalculate("")
                        .expectedResultMessage("Please enter an integer")
                        .build(),
                FactorialTestDataDto.builder()
                        .dataForCalculate("Text")
                        .expectedResultMessage("Please enter an integer")
                        .build()
        );
    }

}

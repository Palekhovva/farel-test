package io.farel.api.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class FactorialApiNegativeTestDataDto {

    /**
     * Строка для расчета
     */
    private String text;

    /**
     * Ожидаемый результат расчета
     */
    private ExpectedResultDto expectedResultDto;

}

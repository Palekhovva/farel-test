package io.farel.api.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class FactorialApiTestDataDto {

    /**
     * Число для расчета
     */
    private Long number;

    /**
     * Ожидаемый результат расчета
     */
    private ExpectedResultDto expectedResultDto;

}

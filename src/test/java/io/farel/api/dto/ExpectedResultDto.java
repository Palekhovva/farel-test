package io.farel.api.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ExpectedResultDto {

    /**
     * Ожидаемый код ответа
     */
    private Integer responseCode;

    /**
     * Ожидаемый формат ответа
     */
    private Class<?> responseClazz;

    /**
     * Ожидаемый ответ
     */
    private Double number;
}

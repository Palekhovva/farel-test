package io.farel.api.steps;

import io.farel.api.dto.ExpectedResultDto;
import io.farel.api.dto.ResponseNumberDto;
import io.farel.core.ApplicationProperties;
import io.farel.function.factorial.enums.RequestParamEnum;
import io.qameta.allure.Step;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

import java.lang.reflect.Type;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class FactorialApiSteps {
    private final ApplicationProperties applicationProperties = ApplicationProperties.getInstance();

    @Step("Запрос на получение факториала для числа {0}")
    public Response factorialRequest(Long number) {
        return getFactorialSpec()
                .formParam(RequestParamEnum.NUMBER.getValue(), number)
                .when()
                .post();
    }

    @Step("Запрос на получение факториала для строки {0}")
    public Response factorialRequestForString(String text) {
        return getFactorialSpec()
                .formParam(RequestParamEnum.NUMBER.getValue(), text)
                .when()
                .post();
    }

    @Step("Проверка результата вычисления факториала")
    public void validateFactorialResponse(Response response, ExpectedResultDto expectedResultDto) {
        assertEquals(expectedResultDto.getResponseCode(), response.getStatusCode(), "Проверка кода ответа");
        ResponseNumberDto responseDto = response.as((Type) expectedResultDto.getResponseClazz());
        assertEquals(expectedResultDto.getNumber(), responseDto.getAnswer(), "Проверка расчитанного числа");

    }

    private RequestSpecification getFactorialSpec() {
        return RestAssured.given()
                .contentType(ContentType.URLENC)
                .baseUri(applicationProperties.getApiUrl())
                .auth()
                .basic(applicationProperties.getUserLogin(), applicationProperties.getUserPassword());
    }
}

package io.farel.api;

import io.farel.api.dto.*;
import io.farel.api.steps.FactorialApiSteps;
import io.restassured.response.Response;
import org.apache.http.HttpStatus;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

public class FactorialApiTest {
    private final FactorialApiSteps factorialApiSteps = new FactorialApiSteps();

    @ParameterizedTest
    @MethodSource("getPositiveTestData")
    public void factorialApiTest(FactorialApiTestDataDto factorialApiTestDataDto) {
        Response response = factorialApiSteps.factorialRequest(factorialApiTestDataDto.getNumber());
        factorialApiSteps.validateFactorialResponse(response, factorialApiTestDataDto.getExpectedResultDto());
    }

    @ParameterizedTest
    @MethodSource("getNegativeTestData")
    public void factorialApiNegativeTest(FactorialApiNegativeTestDataDto factorialApiTestDataDto) {
        Response response = factorialApiSteps.factorialRequestForString(factorialApiTestDataDto.getText());
        factorialApiSteps.validateFactorialResponse(response, factorialApiTestDataDto.getExpectedResultDto());
    }

    private static Stream<FactorialApiTestDataDto> getPositiveTestData() {
        return Stream.of(FactorialApiTestDataDto.builder()
                        .number(-1L)
                        .expectedResultDto(ExpectedResultDto.builder()
                                .number(1d)
                                .responseCode(HttpStatus.SC_NOT_ACCEPTABLE)
                                .responseClazz(ResponseNumberDto.class)
                                .build())
                        .build(),
                FactorialApiTestDataDto.builder()
                        .number(0L)
                        .expectedResultDto(ExpectedResultDto.builder()
                                .number(1d)
                                .responseCode(HttpStatus.SC_OK)
                                .responseClazz(ResponseNumberDto.class)
                                .build())
                        .build(),
                FactorialApiTestDataDto.builder()
                        .number(170L)
                        .expectedResultDto(ExpectedResultDto.builder()
                                .number(7.257415615307999e+306)
                                .responseCode(HttpStatus.SC_OK)
                                .responseClazz(ResponseNumberDto.class)
                                .build())
                        .build(),
                FactorialApiTestDataDto.builder()
                        .number(171L)
                        .expectedResultDto(ExpectedResultDto.builder()
                                .number(Double.POSITIVE_INFINITY)
                                .responseCode(HttpStatus.SC_OK)
                                .responseClazz(ResponseNumberDto.class)
                                .build())
                        .build(),
                FactorialApiTestDataDto.builder()
                        .number(978L)
                        .expectedResultDto(ExpectedResultDto.builder()
                                .number(Double.POSITIVE_INFINITY)
                                .responseCode(HttpStatus.SC_OK)
                                .responseClazz(ResponseNumberDto.class)
                                .build())
                        .build(),
                FactorialApiTestDataDto.builder()
                        .number(979L)
                        .expectedResultDto(ExpectedResultDto.builder()
                                .number(Double.POSITIVE_INFINITY)
                                .responseCode(HttpStatus.SC_OK)
                                .responseClazz(ResponseNumberDto.class)
                                .build())
                        .build());
    }

    private static Stream<FactorialApiNegativeTestDataDto> getNegativeTestData() {
        return Stream.of(FactorialApiNegativeTestDataDto.builder()
                        .text("Text")
                        .expectedResultDto(ExpectedResultDto.builder()
                                .number(0d)
                                .responseCode(HttpStatus.SC_UNSUPPORTED_MEDIA_TYPE)
                                .responseClazz(NegativeResponseDto.class)
                                .build())
                        .build(),
                FactorialApiNegativeTestDataDto.builder()
                        .text("null")
                        .expectedResultDto(ExpectedResultDto.builder()
                                .number(0d)
                                .responseCode(HttpStatus.SC_UNSUPPORTED_MEDIA_TYPE)
                                .responseClazz(NegativeResponseDto.class)
                                .build())
                        .build(),
                FactorialApiNegativeTestDataDto.builder()
                        .text("")
                        .expectedResultDto(ExpectedResultDto.builder()
                                .number(0d)
                                .responseCode(HttpStatus.SC_UNSUPPORTED_MEDIA_TYPE)
                                .responseClazz(NegativeResponseDto.class)
                                .build())
                        .build());
    }
}
